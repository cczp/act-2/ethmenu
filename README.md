# THIS PIECE OF SOFTWARE IS DEPRECATED

Use something else.

---

[ethmenu](https://pypi.org/project/ethmenu/) is a simple dmenu,
dmenu\_run, and i3-dmenu-desktop replacement written in Python that
(in theory, properly) supports both X11 and Wayland. Compared to
dmenu, ethmenu also supports JSON, which allows to add icons and make
captions of options different from their actual printed values (see
`ethmenu(1)` for more details).

## Dependencies

External:

* Python 3
* GTK 3
* make - build-time
* scdoc - build-time for the man pages

PyPI:

* PyGObject
* xdg

## How to install

```
pip3 install [--user] ethmenu
```

## How to customize

See the `ethmenu(5)` man page.

## The menu window isn't floating in tiling window managers

I tried my best to prevent that, but if it does happen, you'll have to
tell your window manager to make it floating. For example, this is how
it's done in sway:

```
for_window [app_id="ethmenu*"] floating enable
```

## Some options are missing

ethmenu-run and ethmenu-desktop may suffer the same problem as
dmenu\_run, and the cache may become malformed. If it somehow happens,
delete the cache files `$XDG_CACHE_HOME/ethmenu-*` or
`~/.cache/ethmenu-*` if `$XDG_CACHE_HOME` isn't set.

## The menu is a little bit slow with *thousands* of options

Yup, it is. And there isn't much I can do with that using GTK. Let's
just pretend it was an essential tradeoff for making it pretty.
