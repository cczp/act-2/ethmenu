#!/usr/bin/env python3

import ethmenu, ethmenu.cache

__title__ = f"{ethmenu.__title__}-run"

ICON = "application-x-executable"

def main():
	"""
	Launches the menu with the list of executables in the $PATH
	directories and executes the chosen one in the shell.
	"""
	
	import subprocess
	
	subprocess.run(ethmenu.real_main(path_option_gen), shell=True)

def path_option_gen():
	"""
	The option generator functionally stolen from dmenu_path.
	
	# Yields
	
	The options of executables in the $PATH directiories.
	"""
	
	import os, xdg
	
	cache_path = xdg.XDG_CACHE_HOME / f"{__title__}"
	
	cached_dirs = os.getenv("PATH", "/bin").split(":")
	
	cache_is_relevant = ethmenu.cache.check(cache_path, cached_dirs)
	
	if cache_is_relevant:
		with open(cache_path, "r") as cache:
			for entry in cache:
				yield ethmenu.Option(caption=entry.rstrip(), icon=ICON)
	else:
		executable_set = set()
		
		for entry in ethmenu.cache.files(cached_dirs):
			if os.access(entry, os.X_OK):
				executable_set.add(os.path.basename(entry))
		
		executables = sorted(executable_set)
		
		with open(cache_path, "w") as cache:
			for entry in executables:
				print(entry, file=cache)
				
				yield ethmenu.Option(caption=entry, icon=ICON)

if __name__ == "__main__":
	main()
