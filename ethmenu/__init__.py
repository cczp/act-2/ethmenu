__title__ = "ethmenu"
__description__ = "Simple menu"
__version__ = "2.0.5"

default_style = bytes("""
decoration, window, entry {
	border-radius: 8px;
}

entry, scrolledwindow {
	margin: 4px;
}

scrolledwindow {
	margin-top: 0;
}

row {
	padding: 4px 8px;
}

image {
	margin-right: 8px;
}
""", "utf-8")

class Option:
	def __init__(self, caption="", value=None, icon=None):
		self.caption = caption
		self.value = value if value != None else caption
		self.icon = icon

def main():
	"""Launches the menu normally."""
	
	print(real_main())

def real_main(option_gen=None):
	"""
	Launches the menu.
	
	# Arguments
	
	* `option_gen` - the generator function that yields the options to
	  be displayed (if none is provided, `stdin_plain_text_option_gen`
	  or `stdin_json_option_gen` is used)
	"""
	
	import os, xdg
	
	args = parse_args(os.sys.argv[1:])
	
	if args.style != None:
		custom_style = read_style_file(args.style, True)
	else:
		custom_style = read_style_file(
			xdg.XDG_CONFIG_HOME / f"{__title__}/style.css",
		False)
	
	if not option_gen:
		if args.json:
			option_gen = stdin_json_option_gen
		else:
			option_gen = stdin_plain_text_option_gen
	
	return setup_menu(args, custom_style, option_gen)

def die(*args, **kwargs):
	"""Prints the error message and closes the program."""
	
	import os
	
	print(f"{os.sys.argv[0]}: error:", *args, **kwargs, file=os.sys.stderr)
	
	os.sys.exit(1)

def stdin_plain_text_option_gen():
	"""
	The default option generator function.
	
	# Yields
	
	The options read from stdin.
	"""
	
	import sys
	
	for line in sys.stdin:
		yield Option(caption=line.rstrip())
def stdin_json_option_gen():
	"""
	The JSON version of the default option generator function.
	
	# Yields
	
	The options read from stdin and parsed as `Option`.
	"""
	
	import sys, json
	
	for option in json.load(sys.stdin):
		yield Option(
			caption=option.get("caption"),
			value=option.get("value"),
			icon=option.get("icon")
		)

def parse_args(args):
	"""
	Parses the command line arguments.
	
	# Arguments
	
	* `args` - the array of the command line arguments
	
	# Returns
	
	The namespace of the parameters.
	"""
	
	import argparse
	
	arg_parser = argparse.ArgumentParser(
		description=__description__,
		epilog=f"See {__title__}(1) and {__title__}(5) for more info."
	)
	
	arg_parser.add_argument("-v", "--version",
		action="version",
		
		version=__title__ + " " + __version__
	)
	
	arg_parser.add_argument("-j", "--json",
		action="store_true",
		
		help="accept JSON input"
	)
	
	arg_parser.add_argument("-i", "--ignore-case",
		action="store_true",
		
		help="ignore input letter case"
	)
	
	arg_parser.add_argument("-s", "--style",
		action="store",
		metavar="FILEPATH",
		
		help="custom stylesheet filepath"
	)
	
	return arg_parser.parse_args(args)

def read_style_file(path, is_custom):
	"""
	Reads the stylesheet file.
	
	# Arguments
	
	* `path` - the filepath of the stylesheet
	* `is_custom` - whether or not the filepath is provided through the
	  command line arguments
	
	# Returns
	
	The byte contents of the file.
	"""
	
	import os, shlex
	
	style = None
	
	try:
		with open(path, "r") as style_file:
			style = bytes(style_file.read(), "utf-8")
	except FileNotFoundError:
		if is_custom:
			die(f"{path}: file not found")
	
	return style

def setup_menu(args, custom_style, option_gen):
	"""
	Opens the menu window.
	
	# Arguments
	
	* `args` - the namespace of the parameters
	* `custom_style` - the contents of the stylesheet
	* `option_gen` - the generator function that yields the options to
	  be displayed
	"""
	
	import re, gi
	
	gi.require_version("Gdk", "3.0")
	gi.require_version("Gtk", "3.0")
	
	from gi.repository import Gdk, Gtk
	
	window = Gtk.Window()
	header_bar = Gtk.HeaderBar()
	
	container = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
	entry = Gtk.SearchEntry()
	option_container = Gtk.ScrolledWindow()
	option_list = Gtk.ListBox()
	
	option_list.filter_regex = re.compile(".*")
	option_list.chosen_option = ""
	
	def option_list_filter_func(row):
		return option_list.filter_regex.match(row.caption)
	
	def entry_on_key_press_event(self, event):
		key = event.keyval
		
		if key in (
			Gdk.KEY_Down,
			Gdk.KEY_Up,
			
			Gdk.KEY_Tab,
			Gdk.KEY_ISO_Left_Tab,
			
			Gdk.KEY_Page_Down,
			Gdk.KEY_Page_Up
		):
			option_list.get_selected_row().grab_focus()
			
			option_list.do_key_press_event(option_list, event)
			
			self.grab_focus_without_selecting()
		elif key == Gdk.KEY_Escape:
			window.destroy()
		elif key == Gdk.KEY_Return:
			selected_row = option_list.get_selected_row()
			
			if selected_row:
				option_list.chosen_option = selected_row.value
			else:
				option_list.chosen_option = self.props.text
			
			window.destroy()
		else:
			return False
		
		return True
	def entry_on_search_changed(self):
		option_list.filter_regex = re.compile(
			".*" + re.escape(
				self.props.text.strip()
			).replace("\\ ", ".*") + ".*",
			
			re.IGNORECASE
			if args.ignore_case
			else 0
		)
		
		option_list.invalidate_filter()
		
		option_list.unselect_all()
	
	def option_list_on_draw(self, _):
		if not self.get_selected_row():
			self.select_row(self.get_row_at_y(0))
	def option_list_on_row_activated(self, row):
		self.chosen_option = row.value
		
		window.destroy()
	
	def option_on_draw(self, _):
		if not self.filled:
			row_container = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
			
			if self.icon:
				icon = Gtk.Image.new_from_icon_name(self.icon, Gtk.IconSize.BUTTON)
				
				icon.set_pixel_size(16)
			
			label = Gtk.Label(label=self.caption)
			
			label.props.xalign = 0
			label.props.wrap = True
			label.props.use_markup = True
			
			if self.icon:
				row_container.add(icon)
			row_container.add(label)
			
			self.add(row_container)
			
			self.show_all()
			
			self.filled = True
	
	
	option_list.set_filter_func(option_list_filter_func)
	
	option_container.props.expand = True
	
	window.props.default_width = 320
	window.props.default_height = 178
	
	window.props.resizable = False
	window.props.skip_taskbar_hint = True
	
	window.props.type_hint = Gdk.WindowTypeHint.DIALOG
	
	for style in (default_style, custom_style):
		if style:
			style_provider = Gtk.CssProvider()
			
			style_provider.load_from_data(style)
			
			Gtk.StyleContext.add_provider_for_screen(
				Gdk.Screen.get_default(),
				
				style_provider,
				Gtk.STYLE_PROVIDER_PRIORITY_USER
			)
	
	for option in option_gen():
		row = Gtk.ListBoxRow()
		
		row.filled = False
		row.caption = option.caption
		row.value = option.value
		row.icon = option.icon
		
		row.connect("draw", option_on_draw)
		
		option_list.insert(row, -1)
	
	option_container.add(option_list)
	
	container.add(entry)
	container.add(option_container)
	
	window.set_titlebar(header_bar)
	window.add(container)
	
	entry.connect("key-press-event", entry_on_key_press_event)
	entry.connect("search-changed", entry_on_search_changed)
	
	option_list.connect("draw", option_list_on_draw)
	option_list.connect("row-activated", option_list_on_row_activated)
	
	window.connect("destroy", Gtk.main_quit)
	
	window.show_all()
	
	# Using a hidden header bar is the only way I know to force GTK to
	# use client-side decorations
	header_bar.hide();
	
	Gtk.main()
	
	return option_list.chosen_option
