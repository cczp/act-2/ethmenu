#!/usr/bin/env python3

import setuptools, ethmenu

setuptools.setup(
	name="ethmenu",
	version=ethmenu.__version__,
	
	packages=setuptools.find_packages(),
	
	data_files=[
		("share/man/man1", ["docs/ethmenu.1"]),
		("share/man/man5", ["docs/ethmenu.5"]),
		
		("share/applications", ["data/ethmenu.desktop"]),
		("share/icons/hicolor/scalable/apps", ["data/ethmenu.svg"])
	],
	
	entry_points={
		"console_scripts": [
			"ethmenu = ethmenu:main",
			"ethmenu-run = ethmenu.run:main",
			"ethmenu-desktop = ethmenu.desktop:main"
		]
	},
	
	install_requires=[
		"PyGObject",
		"xdg"
	],
	
	author="Kirby Kevinson",
	author_email="kirbysemailaddress@protonmail.com",
	
	description=ethmenu.__description__,
	
	long_description=open("README.md", "r").read(),
	long_description_content_type="text/markdown",
	
	keywords="menu dmenu",
	
	url="https://gitlab.com/kirbykevinson/ethmenu",
	
	classifiers=[
		"Development Status :: 5 - Production/Stable",
		"Environment :: X11 Applications :: GTK",
		"License :: OSI Approved :: ISC License (ISCL)",
		"Operating System :: POSIX",
		"Programming Language :: Python :: 3",
		"Topic :: Desktop Environment :: Window Managers"
	]
)
